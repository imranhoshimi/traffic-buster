using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadController : MonoBehaviour {

	public int roadLength = 100;    //	meters
	public GameObject roadPrefab;
	public List<GameObject> roadsPool;

	// Use this for initialization
	void Start () {
		GenerateRoads();
	}
	
	void GenerateRoads()
	{		
		for(int i=0; i<roadLength; i++)
		{
			GameObject goRoad = Instantiate(roadPrefab, transform);
			goRoad.transform.localPosition = new Vector3(0f, 0f, i * 5f);
			roadsPool.Add(goRoad);
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
