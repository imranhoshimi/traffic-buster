using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadPatch : MonoBehaviour {

	public Rigidbody rb;
	public Collider groundCollider;       //This stores a reference to the collider attached to the Ground.
	public float groundHorizontalLength;

	void Awake()
	{
		//Get and store a reference to the collider2D attached to Ground.
		groundCollider = GetComponent<Collider>();
		//Store the size of the collider along the x axis (its length in units).
		groundHorizontalLength = groundCollider.bounds.size.z;
	}

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();

		//Start the object moving.
		rb.velocity = new Vector3(0f, 0f, -GameManager.Current.scrollSpeed);
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.Current.gameOver == true)
		{
			//rb.velocity = Vector3.zero;
			//return;
		}

		//Check if the difference along the x axis between the main Camera and the position of the object this is attached to is greater than groundHorizontalLength.
		if (transform.position.z < -groundHorizontalLength)
        {
			//If true, this means this object is no longer visible and we can safely move it forward to be re-used.
			Reposition();
		}
	}

	//Moves the object this script is attached to right in order to create our looping background effect.
	private void Reposition()
	{
		//This is how far to the right we will move our background object, in this case, twice its length. This will position it directly to the right of the currently visible background object.
		Vector3 groundOffSet = new Vector3(0f, 0f, groundHorizontalLength * 25f);

		//Move this object from it's position offscreen, behind the player, to the new position off-camera in front of the player.
		transform.position = transform.position + groundOffSet;
	}
}
