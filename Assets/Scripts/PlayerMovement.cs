using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	Rigidbody player;

	private void Awake()
	{
		player = GetComponent<Rigidbody>();
	}
	private void FixedUpdate()
	{
		if (player == null)
			return;

        Move();
	}

    Vector2 delta = Vector2.zero;
    Vector2 lastPos = Vector2.zero;
    public float swipeSpeed = 0f;
    void Update()
    {
        EditorInput();
    }

    public float MoveSpeed = 1f;
    public float MoveTime = 1f;
    private void Move()
    {
        // Move player on the Z axis (sideways)
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || delta.normalized.x > 0)
        {
            player.AddForce(MoveSpeed * swipeSpeed * Time.deltaTime, 0f, 0f, ForceMode.VelocityChange);
        }
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) || delta.normalized.x < 0)
        {
            player.AddForce(-MoveSpeed * swipeSpeed * Time.deltaTime, 0f, 0f, ForceMode.VelocityChange);
        }
    }

    float lastTime = 0f;
    void EditorInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            lastPos = (Vector2)Input.mousePosition;
            lastTime = Time.time;
        }
        else if (Input.GetMouseButton(0))
        {
            float elapsedTime = Time.time - lastTime;
            if (elapsedTime >= 0.1f)
            {
                lastPos = (Vector2)Input.mousePosition;
                lastTime = Time.time;
            }

            delta = (Vector2)Input.mousePosition - lastPos;

            Debug.Log("delta X : " + delta.x);
            Debug.Log("delta Y : " + delta.y);

            Debug.Log("delta distance : " + delta.magnitude);

            Vector2 mouseAxis = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
            swipeSpeed = mouseAxis.magnitude / Time.deltaTime;

        }
        else if (Input.GetMouseButtonUp(0))
        {
            delta = Vector2.zero;
            swipeSpeed = 0f;
            lastTime = 0f;
            lastPos = Vector2.zero;
        }
    }

    public AudioSource audioSource;
    public AudioClip audioClip;
    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Obstacle":
                if (audioSource.isPlaying)
                    audioSource.Stop();
                audioSource.PlayOneShot(audioClip);
                break;
        }
    }
}
