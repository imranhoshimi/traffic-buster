using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadScroll : MonoBehaviour {
    
	public Vector3 forceVector = Vector3.zero;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	private void Update()
	{
        // Move obstacle on Z axis (towards player)
		transform.position += forceVector * Time.deltaTime;
		if (transform.position.z < -40)
            transform.position = new Vector3(transform.localPosition.x, 0, 120);
	}
}
